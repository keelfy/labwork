#ifndef SET_H
#define SET_H

#include <iostream>
#include "sequence.h"

class Set : public Sequence {
private:
    int* arr;
    unsigned int current_size;

    /**
     * @brief Создает множество указанного размера
     * @param size - размер
     */
    void create(unsigned int size) override;
public:
    /**
     * @brief Конструктор, создающий пустое множество
     */
    Set();
    /**
     * @brief Копирующий конструктор
     * @param set - множество для копироваия
     */
    Set(Set& set);
    /**
     * @brief Конструктор, создающий множество из переданных данных
     * @param array - массив чисел
     * @param size - размер массива
     */
    Set(int* array, unsigned int size);
    /**
     * @brief Конструктор, создающий пустое множество с указанным размером
     * @param size - размер
     */
    Set(unsigned int size);
    /**
     * @brief Пересоздает это множество из переданного
     * @param set - множество для копирования
     * @return this
     */
    Set& from(Set& set);
    /**
     * @brief Добавляет переданное множество к этому
     * @param set - множество для добавления
     * @return this
     */
    Set& append(Set& set);
    /** @see Sequence::get() */
    int get(unsigned int index) override;
    /** @see Sequence::set() */
    void set(unsigned int index, int value) override;
    /** @see Sequence::add() */
    void add(int value) override;
    /** @see Sequence::size() */
    unsigned int size() override;
    /** @see Sequence::isEmpty() */
    bool isEmpty() override;
    /** @see Sequence::clear() */
    void clear() override;
    /**
     * @brief Проверяет, присутствует ли во множестве значение
     * @param value - значение
     * @return статус
     */
    bool contains(int value);
    /**
     * @brief Сортирует множество по возрастанию
     * @return this
     */
    Set* sort();
    /**
     * @brief Умножает это множество на переданное
     * @param set - умножаемое
     * @return this
     */
    Set& multiply(Set& set);
    /**
     * @brief вычитает из этого множество переданное
     * @param set - вычитаемое
     * @return this
     */
    Set& subtract(Set& set);

    ~Set() override;

    /** Перегрузка оператора взятия по индексу */
    int operator [](unsigned int index);

    /** Перегрузка сложения с множеством с присвоением */
    Set& operator +=(Set& b);

    /** Перегрузка сложения с числом с присвоением */
    Set& operator +=(int b);

    /** Перегрузка умножения с присвоением */
    Set& operator *=(Set& b);

    /** Перегрузка вычитания с присвоением */
    Set& operator -=(Set& b);

    /** Перегрузка оператора присвоения множества */
    Set& operator =(Set& b);

    /** Перегрузка эквивалентности - проверяет множества поэлементно */
    bool operator ==(Set& b);

    /** Перегрузка неэквивалентности - проеряет множества поэлементно */
    bool operator !=(Set& b);

    /** Перегрузка оператора меньше - сравнивает размеры */
    bool operator <(Set& b);

    /** Перегрузка оператора больше - сравнивает размеры */
    bool operator >(Set& b);

    /** Перегрузка оператора меньше или равно - сравнивает размеры */
    bool operator <=(Set& b);

    /** Перегрузка оператора больше или равно - сравнивает размеры */
    bool operator >=(Set& b);

    /** Перегрузка оператора потоковго вывода */
    friend std::ostream& operator <<(std::ostream& os, Set& set);

    /** Перегрузка оператора сложения - логическое ИЛИ */
    friend Set& operator +(Set& a, Set& b);

    /** Перегрузка оператора сложения с числом - добавление элемента в множество */
    friend Set& operator +(Set& a, int b);

    /** Перегрузка оператора умножения - логическое И */
    friend Set& operator *(Set& a, Set& b);

    /** Перегрузка оператора вычитания - возвращает значения первого множества, которых нет во втором */
    friend Set& operator -(Set& a, Set& b);
};

#endif
