#include <iostream>
#include "linkedlist.h"

struct LinkedList::Node {
    int value;
    Node *next;

    Node(int value, Node *next = nullptr) {
        this->next = next;
        this->value = value;
    }
};

LinkedList::LinkedList() {
    this->create(0);
}

LinkedList::LinkedList(unsigned int size) {
    this->create(size);
}

LinkedList::LinkedList(LinkedList* list) {
    this->from(*list);
}

LinkedList::~LinkedList() {
    this->clear();
}

void LinkedList::create(unsigned int size) {
    this->current_size = size;
    this->head = nullptr;
}

LinkedList& LinkedList::from(LinkedList& list) {
    this->clear();
    Node *current = list.head;
    while (current != nullptr) {
        this->add(current->value);
        current = current->next;
    }
    return *this;
}

LinkedList& LinkedList::append(LinkedList& list) {
    for (unsigned int i = 0; i < list.size(); i++)
        this->add(list.get(i));
    return *this;
}


void LinkedList::set(unsigned int index, int value) {
    if (index >= this->size())
        throw std::out_of_range("OutOfBounds");

    Node* current = this->head;
    for (unsigned int i = 0; i < index; i++) {
        current = current->next;
    }
    current->value = value;
}

int LinkedList::get(unsigned int index) {
    if (index >= this->size())
        throw std::out_of_range("OutOfBounds");

    Node* current = this->head;
    for (unsigned int i = 0; i < index; i++) {
        current = current->next;
    }
    return current->value;
}

void LinkedList::add(int value) {    
    if (head == nullptr)
        head = new Node(value);
    else {
        Node* current = this->head;

        while (current->next != nullptr)
            current = current->next;

        current->next = new Node(value);
    }
    ++this->current_size;
}

void LinkedList::removeFirst() {
    Node *temp = this->head;
    this->head = head->next;
    delete temp;
    --this->current_size;
}

void LinkedList::removeAt(unsigned int index) {
    if (index == 0)
        removeFirst();
    else {
        Node* previous = this->head;
        for (unsigned int i = 0; i < index - 1; i++)
            previous = previous->next;

        Node* toDelete = previous->next;
        previous->next = toDelete->next;
        delete toDelete;
        --this->current_size;
    }
}

void LinkedList::addFirst(int value) {
    head = new Node(value, head);
    ++this->current_size;
}

void LinkedList::addAt(unsigned int index, int value) {
    if (index == 0)
        addFirst(value);
    else {
        Node* previous = this->head;

        for (unsigned int i = 0; i < index - 1; i++)
            previous = previous->next;

        Node* newNode = new Node(value, previous->next);
        previous->next = newNode;
        ++this->current_size;
    }
}

unsigned int LinkedList::size() {
    return this->current_size;
}

bool LinkedList::isEmpty() {
    return this->size() == 0;
}

void LinkedList::clear() {
    while (!isEmpty()) {
        removeFirst();
    }
}

bool LinkedList::contains(int value) {
    if (isEmpty())
        return false;

    LinkedList *sorted = sort();
    unsigned int left = 0;
    unsigned int right = this->size() - 1;
    unsigned int middle;
    while (left < right) {
        middle = (left + right) / 2;

        if (sorted->get(middle) < value)
            left = middle + 1;
        else
            right = middle;
    }
    return sorted->get(left) == value;
}

LinkedList* LinkedList::sort() {
    LinkedList *list = this;
    Node *node = nullptr;
    Node *temp = nullptr;
    int tempvar;
    node = list->head;
    while (node != nullptr) {
        temp = node;
        while (temp->next != nullptr) {
            if(temp->value > temp->next->value) {
                tempvar = temp->value;
                temp->value = temp->next->value;
                temp->next->value = tempvar;
            }
            temp = temp->next;
        }
        node = node->next;
    }
    return list;
}

LinkedList& LinkedList::multiply(LinkedList& list) {
    LinkedList *result = new LinkedList();
    for (unsigned int i = 0; i < this->size(); i++) {
        int el = this->get(i);
        if (list.contains(el))
            result->add(el);
    }
    return *result;
}

LinkedList& LinkedList::subtract(LinkedList& set) {
    LinkedList *result = new LinkedList();
    for (unsigned int i = 0; i < this->size(); i++)
        if (!set.contains(this->get(i)))
            result->add(this->get(i));
    return *result;
}

LinkedList& operator +(LinkedList& a, LinkedList& b) {
    LinkedList *list = new LinkedList(a);
    return list->append(b);
}

LinkedList& operator +(LinkedList &a, int b) {
    LinkedList *list = new LinkedList(a);
    list->add(b);
    return *list;
}

LinkedList& operator *(LinkedList& a, LinkedList& b) {
    LinkedList *list = new LinkedList(a);
    return list->multiply(b);
}

LinkedList& operator -(LinkedList& a, LinkedList& b) {
    LinkedList *list = new LinkedList(a);
    return list->subtract(b);
}

LinkedList& LinkedList::operator +=(LinkedList& b) {
    if (this == &b)
        return *this;

    return this->append(b);
}

LinkedList& LinkedList::operator +=(int b) {
    this->add(b);
    return *this;
}

LinkedList& LinkedList::operator *=(LinkedList& b) {
    if (this == &b)
        return *this;

    return this->from(this->multiply(b));
}

LinkedList& LinkedList::operator -=(LinkedList& b) {
    if (this == &b)
        return *this;
    return this->from(subtract(b));
}

LinkedList& LinkedList::operator =(LinkedList& b) {
    if (this == &b)
        return *this;

    return this->from(b);
}

bool LinkedList::operator ==(LinkedList& b) {
    if (this->size() != b.size())
        return false;

    for (unsigned int i = 0; i < this->size(); i++)
        if (this->get(i) != b.get(i))
            return false;
    return true;
}

bool LinkedList::operator !=(LinkedList& b) {
    return !(this == &b);
}

bool LinkedList::operator <(LinkedList& b) {
    return this->size() < b.size();
}

bool LinkedList::operator >(LinkedList& b) {
    return this->size() > b.size();
}

bool LinkedList::operator <=(LinkedList& b) {
    return this->size() <= b.size();
}

bool LinkedList::operator >=(LinkedList& b) {
    return this->size() >= b.size();
}

int LinkedList::operator [](unsigned int index) {
    return get(index);
}

std::ostream& operator <<(std::ostream& os, LinkedList& list) {
    if (list.isEmpty()) {
        os << "{ empty }" << std::endl;
        return os;
    }
    os << "{ ";
    for (unsigned int i = 0; i < list.size(); i++)
        os << list.get(i) << (i + 1 == list.size() ? "" : ", ");
    os << " }";
    return os;
}
