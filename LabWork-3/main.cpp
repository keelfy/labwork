#include <iostream>
#include "set.h"
#include "linkedlist.h"

int main()
{
    std::cout << "--- SET ---" << std::endl << std::endl;

    Set set;
    set.add(400);
    set += 20;
    std::cout << "set_1 = " << set << std::endl;

    Set set1;
    set1.add(10);
    set1.add(20);
    set1 += 15;
    std::cout << "set_2 = " << set1 << std::endl << std::endl;

    std::cout << "Is set_1 equal set_2? => " << ((set == set1) ? "True" : "False") << std::endl << std::endl;

    Set set2;
    Set set3;

    set2 = set * set1;
    std::cout << "set_1 * set_2 => " << set2 << std::endl;

    set3 = set1;
    set3 *= set;
    std::cout << "set_2 *= set_1 => " << set3 << std::endl;

    set2 = set + set1;
    std::cout << "set_1 + set_2 => "<< set2 << std::endl;

    set3 = set1;
    set3 += set;
    std::cout << "set_1 += set_2 => " << set3 << std::endl;

    set2 = set1 - set;
    std::cout << "set_2 - set_1 => "<< set2 << std::endl;

    set3 = set1;
    set3 -= set;
    std::cout << "set_2 -= set_1 => " << set3 << std::endl;

    std::cout << "\n\n--- LINKED LIST ---\n\n";

    LinkedList list;
    list.add(400);
    list += 20;
    std::cout << "list_1 = " << list << std::endl;

    LinkedList list1;
    list1.add(10);
    list1.add(20);
    list1 += 15;
    std::cout << "list_2 = " << list1 << std::endl << std::endl;

    std::cout << "Is list_1 equal list_2? => " << ((list == list1) ? "True" : "False") << std::endl << std::endl;

    LinkedList list2;
    LinkedList list3;

    list2 = list * list1;
    std::cout << "list_1 * list_2 => " << list2 << std::endl;

    list3 = list1;
    list3 *= list;
    std::cout << "list_2 *= list_1 => " << list3 << std::endl;

    list2 = list + list1;
    std::cout << "list_1 + list_2 => "<< list2 << std::endl;

    list3 = list1;
    list3 += list;
    std::cout << "list_1 += list_2 => " << list3 << std::endl;

    list2 = list1 - list;
    std::cout << "list_2 - list_1 => "<< list2 << std::endl;

    list3 = list1;
    list3 -= list;
    std::cout << "list_2 -= list_1 => " << list3 << std::endl;

    return 0;
}
