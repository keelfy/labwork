import qbs

CppApplication {
    Properties {
        cpp.cxxLanguageVersion: "c++11"
    }

    consoleApplication: true
    files: [
        "main.cpp",
        "sequence.h",
        "set.h",
        "linkedlist.h",
        "sequence.cpp",
        "set.cpp",
        "linkedlist.cpp"
    ]

    Group {     // Properties for the produced executable
        fileTagsFilter: "application"
        qbs.install: true
        qbs.installDir: "bin"
    }
}
