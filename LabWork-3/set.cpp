#include <iostream>
#include "set.h"

Set::Set() {
    this->create(0);
}

Set::Set(unsigned int size) {
    this->create(size);
}

Set::Set(Set& set) {
    this->from(set);
}

Set::Set(int* array, unsigned int size) {
    this->arr = array;
    this->current_size = size;
}

Set& Set::from(Set& set) {
    this->create(set.size());
    for (unsigned int i = 0; i < set.size(); i++)
        this->set(i, set.get(i));
    return *this;
}

Set& Set::append(Set& set) {
    for (unsigned int i = 0; i < set.size(); i++)
        this->add(set.get(i));

    return *this;
}

Set::~Set() {
    delete [] arr;
}

void Set::create(unsigned int size) {
    this->current_size = size;
    this->arr = new int[size];
}

int Set::get(unsigned int index) {
    if (index >= size())
        throw std::out_of_range("OutOfBounds");

    return this->arr[index];
}

void Set::set(unsigned int index, int value) {
    if (index >= current_size)
        throw std::out_of_range("OutOfBounds");

    this->arr[index] = value;
}

void Set::add(int value) {
    if (this->contains(value))
        return;

    int* buffer = this->arr;
    this->create(this->current_size + 1);

    for (unsigned int i = 0; i < this->size() - 1; i++) {
        this->set(i, buffer[i]);
    }

    arr[this->size() - 1] = value;
}

unsigned int Set::size() {
    return current_size;
}

bool Set::isEmpty() {
    return current_size == 0;
}

void Set::clear() {
    delete [] arr;
    this->create(0);
}

bool Set::contains(int value) {
    if (isEmpty())
        return false;

    Set *sorted = sort();
    unsigned int left = 0;
    unsigned int right = this->size() - 1;
    unsigned int middle;
    while (left < right) {
        middle = (left + right) / 2;

        if (sorted->get(middle) < value)
            left = middle + 1;
        else
            right = middle;
    }
    return sorted->get(left) == value;
}

Set* Set::sort() {
    int* arr1 = arr;
    int buffer;
    for (unsigned int i = 0; i < this->size() - 1; i++) {
        for (unsigned int j = 0; j < this->size() - i - 1; j++) {
            if (arr1[j] > arr1[j + 1]) {
                buffer = arr1[j];
                arr1[j] = arr1[j + 1];
                arr1[j + 1] = buffer;
            }
        }
    }
    return new Set(arr1, this->size());
}

Set& Set::multiply(Set& set) {
    Set *result = new Set();
    for (unsigned int i = 0; i < this->size(); i++) {
        int el = this->get(i);
        if (set.contains(el))
            result->add(el);
    }
    return *result;
}

Set& Set::subtract(Set& set) {
    Set *result = new Set();
    for (unsigned int i = 0; i < this->size(); i++)
        if (!set.contains(this->get(i)))
            result->add(this->get(i));
    return *result;
}

Set& operator +(Set& a, Set& b) {
    Set *set = new Set(a);
    return set->append(b);
}

Set& operator +(Set &a, int b) {
    Set *set = new Set(a);
    set->add(b);
    return *set;
}

Set& operator *(Set& a, Set& b) {
    Set *set = new Set(a);
    return set->multiply(b);
}

Set& operator -(Set& a, Set& b) {
    Set *set = new Set(a);
    return set->subtract(b);
}

Set& Set::operator +=(Set& b) {
    if (this == &b)
        return *this;

    return this->append(b);
}

Set& Set::operator +=(int b) {
    this->add(b);
    return *this;
}

Set& Set::operator *=(Set& b) {
    if (this == &b)
        return *this;

    return this->from(this->multiply(b));
}

Set& Set::operator -=(Set& b) {
    if (this == &b)
        return *this;

    return this->from(subtract(b));
}

Set& Set::operator =(Set& b) {
    if (this == &b)
        return *this;

    this->arr = b.arr;
    this->current_size = b.size();
    return *this;
}

bool Set::operator ==(Set& b) {    
    if (this->size() != b.size())
        return false;

    for (unsigned int i = 0; i < this->size(); i++)
        if (this->get(i) != b.get(i))
            return false;
    return true;
}

bool Set::operator !=(Set& b) {
    return !(this == &b);
}

bool Set::operator <(Set& b) {
    return this->size() < b.size();
}

bool Set::operator >(Set& b) {
    return this->size() > b.size();
}

bool Set::operator <=(Set& b) {
    return this->size() <= b.size();
}

bool Set::operator >=(Set& b) {
    return this->size() >= b.size();
}

int Set::operator[](unsigned int index) {
    return get(index);
}

std::ostream& operator <<(std::ostream& os, Set& set) {
    if (set.isEmpty()) {
        os << "{ empty }" << std::endl;
        return os;
    }
    os << "{ ";
    for (unsigned int i = 0; i < set.size(); i++)
        os << set[i] << (i + 1 == set.size() ? "" : ", ");
    os << " }";
    return os;
}
