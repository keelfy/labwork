#ifndef SEQUENCE_H
#define SEQUENCE_H

class Sequence {
protected:
    virtual void create(unsigned int size) = 0;
public:
    /**
     * @brief Получения значения по индексу
     * @param index - порядковый номер
     * @return значение
     */
    virtual int get(unsigned int index) = 0;
    /**
     * @brief Устанавливает значение для элемента по индексу
     * @param index - порядковый номер
     * @param value - новое значение
     */
    virtual void set(unsigned int index, int value) = 0;
    /**
     * @brief Добавляет элемент
     * @param value - значение
     */
    virtual void add(int value) = 0;
    /**
     * @brief Получение размера последовательности
     * @return размер
     */
    virtual unsigned int size() = 0;
    /**
     * @brief Проверка последовательности на пустоту
     * @return статус
     */
    virtual bool isEmpty() = 0;
    /** @brief Очищает последовательность */
    virtual void clear() = 0;
    /** @brief Деструктор */
    virtual ~Sequence() = 0;
};

#endif
