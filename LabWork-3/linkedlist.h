#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <iostream>
#include "sequence.h"

class LinkedList : public Sequence {
private:
    struct Node;
    Node *head;
    unsigned int current_size;

    /**
     * @brief Создает список указанного размера
     * @param size - размер
     */
    void create(unsigned int size) override;
public:
    /**
     * @brief Конструктор, создающий пустой список
     */
    LinkedList();
    /**
     * @brief Копирующий конструктор
     * @param set - список для копироваия
     */
    LinkedList(LinkedList* set);
    /**
     * @brief Конструктор, создающий пустой список с указанным размером
     * @param size - размер
     */
    LinkedList(unsigned int size);
    /**
     * @brief Пересоздает этот список из переданного
     * @param set - список для копирования
     * @return this
     */
    LinkedList& from(LinkedList& set);
    /**
     * @brief Добавляет переданный список к этому
     * @param set - список для добавления
     * @return this
     */
    LinkedList& append(LinkedList& set);

    /**
     * @brief Очищающий деструктор
     */
    ~LinkedList() override;

    /** @see Sequence::get() */
    int get(unsigned int index) override;
    /** @see Sequence::set() */
    void set(unsigned int index, int value) override;
    /** @see Sequence::add() */
    void add(int value) override;
    /** @see Sequence::size() */
    unsigned int size() override;
    /** @see Sequence::isEmpty() */
    bool isEmpty() override;
    /** @see Sequence::clear() */
    void clear() override;

    /**
     * @brief Удаляет первый элемент списка
     */
    void removeFirst();
    /**
     * @brief Удаляет элемент по индексу
     * @param index - индекс
     */
    void removeAt(unsigned int index);

    /**
     * @brief Добавляет элемент на первое место в списке
     * @param value - значение
     */
    void addFirst(int value);
    /**
     * @brief Добавляет элемент на позицию
     * @param index - позиция
     * @param value - значение
     */
    void addAt(unsigned int index, int value);

    /**
     * @brief Проверяет, присутствует ли в списке значение
     * @param value - значение
     * @return статус
     */
    bool contains(int value);
    /**
     * @brief Сортирует список по возрастанию
     * @return this
     */
    LinkedList* sort();
    /**
     * @brief Умножает этот список на переданное
     * @param set - умножаемое
     * @return this
     */
    LinkedList& multiply(LinkedList& set);
    /**
     * @brief Вычитает из этого списка переданное
     * @param set - вычитаемое
     * @return this
     */
    LinkedList& subtract(LinkedList& set);

    /** Перегрузка оператора взятия по индексу */
    int operator [](unsigned int index);

    /** Перегрузка сложения с множеством с присвоением */
    LinkedList& operator +=(LinkedList& b);

    /** Перегрузка сложения с числом с присвоением */
    LinkedList& operator +=(int b);

    /** Перегрузка умножения с присвоением */
    LinkedList& operator *=(LinkedList& b);

    /** Перегрузка вычитания с присвоением */
    LinkedList& operator -=(LinkedList& b);

    /** Перегрузка оператора присвоения множества */
    LinkedList& operator =(LinkedList& b);

    /** Перегрузка эквивалентности - проверяет списки поэлементно */
    bool operator ==(LinkedList& b);

    /** Перегрузка неэквивалентности - проеряет списки поэлементно */
    bool operator !=(LinkedList& b);

    /** Перегрузка оператора меньше - сравнивает размеры */
    bool operator <(LinkedList& b);

    /** Перегрузка оператора больше - сравнивает размеры */
    bool operator >(LinkedList& b);

    /** Перегрузка оператора меньше или равно - сравнивает размеры */
    bool operator <=(LinkedList& b);

    /** Перегрузка оператора больше или равно - сравнивает размеры */
    bool operator >=(LinkedList& b);

    /** Перегрузка оператора потоковго вывода */
    friend std::ostream& operator <<(std::ostream& os, LinkedList& list);

    /** Перегрузка оператора сложения - логическое ИЛИ */
    friend LinkedList& operator +(LinkedList& a, LinkedList& b);

    /** Перегрузка оператора сложения с числом - добавление элемента в список */
    friend LinkedList& operator +(LinkedList& a, int b);

    /** Перегрузка оператора умножения - логическое И */
    friend LinkedList& operator *(LinkedList& a, LinkedList& b);

    /** Перегрузка оператора вычитания - возвращает значения первого списка, которых нет во втором */
    friend LinkedList& operator -(LinkedList& a, LinkedList& b);
};

#endif
