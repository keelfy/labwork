#include <iostream>
#include <list>
#include <string>
#include <algorithm>
#include <fstream>

using namespace std;

class SortedSet {
private:
    std::list<string> list;

    void create(unsigned int size = 0, string value = "") {
        list.assign(size, value);
    }
public:
    SortedSet() {
        this->create(0);
    }

    SortedSet(unsigned int size) {
        this->create(size);
    }

    SortedSet(SortedSet& set) {
        this->from(set);
    }

    SortedSet& from(SortedSet& set) {
        this->clear();
        for (unsigned int i = 0; i < set.size(); i++) {
            string el = set.get(i);

            if (this->contains(el))
                continue;

            this->add(el);
        }
        this->sort();
        return *this;
    }

    SortedSet& append(SortedSet& set) {
        for (unsigned int i = 0; i < set.size(); i++) {
            string el = set.get(i);

            if (this->contains(el))
                continue;

            this->add(el);
        }
        this->sort();
        return *this;
    }

    ~SortedSet() {
        clear();
    }

    string get(unsigned int index) {
        if (index >= size())
            throw std::out_of_range("OutOfBounds");

        std::list<string>::iterator it = list.begin();
        std::advance(it, index);
        return *it;
    }

    void set(unsigned int index, string value) {
        if (index >= size())
            throw std::out_of_range("OutOfBounds");

        if (this->contains(value))
            return;

        std::list<string>::iterator it = list.begin();
        std::advance(it, index);
        this->list.erase(it);
        this->list.push_back(value);
        this->sort();
    }

    void add(string value) {
        if (this->contains(value))
            return;

        this->list.push_back(value);
        this->sort();
    }

    unsigned int size() {
        return list.size();
    }

    bool isEmpty() {
        return list.empty();
    }

    void clear() {
        list.clear();
    }

    bool contains(string value) {
        if (isEmpty())
            return false;

        return (std::find(list.begin(), list.end(), value) != list.end());
    }

    SortedSet& sort() {
        this->list.sort();
        return *this;
    }

    SortedSet* multiply(SortedSet* set) {
        SortedSet* result = new SortedSet();
        for (unsigned int i = 0; i < this->size(); i++) {
            string el = this->get(i);
            if (set->contains(el))
                result->add(el);
        }
        return result;
    }

    SortedSet* subtract(SortedSet* set) {
        SortedSet* result = new SortedSet();
        for (unsigned int i = 0; i < this->size(); i++)
            if (!set->contains(this->get(i)))
                result->add(this->get(i));
        return result;
    }

    SortedSet& operator =(SortedSet& b) {
        if (this == &b)
            return *this;

        return this->from(b);
    }

    friend std::ostream& operator <<(std::ostream& os, SortedSet& set);
};

std::ostream& operator <<(std::ostream& os, SortedSet& set) {
    if (set.isEmpty()) {
        os << "{ empty }" << std::endl;
        return os;
    }
    os << "{ ";
    for (unsigned int i = 0; i < set.size(); i++)
        os << set.get(i) << (i + 1 == set.size() ? "" : ", ");
    os << " }";
    return os;
};

/**
 * @brief Getting all words contained in file specified by path
 *
 * @param path - path to file
 * @return sorted set of words
 */
SortedSet* get_file_words(string path) {

    ifstream file(path);
    SortedSet* words = new SortedSet();

    string word;
    while (!file.eof()) {
        file >> word;

        if (word != "")
            words->add(word);
    }

    file.close();
    return words;
};

/**
 * @brief Getting all lines contained in file specified by path
 *
 * @param path - path to file
 * @return sorted set of lines
 */
SortedSet* get_file_lines(string path) {

    ifstream file(path);
    SortedSet* lines = new SortedSet();

    string line;
    while (!file.eof()) {
        getline(file, line);

        if (line != "")
            lines->add(line);
    }

    file.close();
    return lines;
};

/**
 * @brief Mupltiplies words and lines from first file to seconds file and writes it to output file.
 *
 * @param path1 - path to first file
 * @param path2 - path to second file
 * @param out - path to file with multiplication result
 */
void multiply_file_text(string path1, string path2, string out) {

    SortedSet* words = get_file_words(path1)->multiply(get_file_words(path2));
    SortedSet* lines = get_file_lines(path1)->multiply(get_file_lines(path2));

    ofstream file(out);

    file << "--- MULTIPLIED WORDS ---\n";
    for (unsigned int i = 0; i < words->size(); i++) {
        file << words->get(i) << std::endl;
    }

    file << "\n--- MULTIPLIED LINES ---\n";
    for (unsigned int i = 0; i < lines->size(); i++) {
        file << lines->get(i) << std::endl;
    }

    delete words;
    delete lines;
    file.close();
};

/**
 * @brief Subtracts words and lines from first file by second and writes it to output file.
 *
 * @param path1 - path to first file
 * @param path2 - path to second file
 * @param out - path to file with subtraction result
 */
void subtract_file_text(string path1, string path2, string out) {

    SortedSet* words = get_file_words(path1)->subtract(get_file_words(path2));
    SortedSet* lines = get_file_lines(path1)->subtract(get_file_lines(path2));

    ofstream file(out);

    file << "--- SUBTRACTED WORDS ---\n";
    for (unsigned int i = 0; i < words->size(); i++) {
        file << words->get(i) << std::endl;
    }

    file << "\n--- SUBTRACTED LINES ---\n";
    for (unsigned int i = 0; i < lines->size(); i++) {
        file << lines->get(i) << std::endl;
    }

    delete words;
    delete lines;
    file.close();
};

/// Tests
int main()
{
    string in1, in2, out1, out2;

    std::cout << "Type first file name: ";
    getline(std::cin, in1);

    ifstream file1(in1);
    if (!file1.good()) {
        std::cout << "File " + in1 + " not found!" << endl;
        return -1;
    }
    file1.close();

    std::cout << "Type second file name: ";
    getline(std::cin, in2);

    ifstream file2(in2);
    if (!file2.good()) {
        std::cout << "File " + in2 + " not found!" << endl;
        return -1;
    }
    file2.close();

    std::cout << "Multiplication output file: ";
    getline(std::cin, out1);

    std::cout << "Subtraction output file: ";
    getline(std::cin, out2);

    multiply_file_text(in1, in2, out1);
    std::cout << "\nMultiplication result written to " + out1 + ".\n";

    subtract_file_text(in1, in2, out2);
    std::cout << "\nSubtraction result written to " + out2 + ".\n";

    return 0;
}
