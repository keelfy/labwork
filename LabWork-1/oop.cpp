#include <iostream>

class Set {
    unsigned int current_size;
    int* arr;

    /** Cоздает динамический массив */
    void create(unsigned int size) {
        this->current_size = size;
        this->arr = new int[size];
    }

public:
    /** Конструктор создает пустое множество */
    Set() {
        this->create(0);
    }

    /** Деструктор удаляет массив */
    ~Set() {
        delete [] arr;
    }

    /** Пересоздает это множество из переданного */
    void from(Set* set) {
        this->clear();
        for (unsigned int i = 0; i < set->size(); i++) {
            this->add(set->get(i));
        }
    }

    /** Добавляет переданное множество к этому */
    void append(Set* set) {
        for (unsigned int i = 0; i < set->size(); i++) {
            this->add(set->get(i));
        }
    }

    /** Возвращает текущий размер множества */
    unsigned int size() {
        return current_size;
    }

    /** Возвращает элемент по индексу */
    int get(unsigned int index) {
        if (index >= current_size)
            throw "OutOfBounds Exception";

        return this->arr[index];
    }

    /** Добавляет значение в множество */
    void add(int value) {
        if (this->contains(value))
            return;

        int* buffer = new int[current_size];
        buffer = arr;
        this->arr = new int[current_size + 1];

        for (unsigned int i = 0; i < current_size; i++)
            this->arr[i] = buffer[i];

        arr[current_size++] = value;
    }

    /** Выводит элементы множества в консоль */
    void log() {
        if (isEmpty()) {
            std::cout << "{ emtpty }" << std::endl;
            return;
        }

        std::cout << "{ ";
        for (unsigned int i = 0; i < current_size; i++){
            std::cout << arr[i] << (i + 1 == current_size ? "" : ", ");
        }
        std::cout << " }" << std::endl;
    }

    /** Проверяет наличие элемента в множестве */
    bool contains(int value) {
        if (isEmpty())
            return false;

        int* sorted = sort();
        unsigned int left = 0;
        unsigned int right = size() - 1;
        unsigned int middle;
        while (left < right) {
            middle = (left + right) / 2;

            if (sorted[middle] < value)
                left = middle + 1;
            else
                right = middle;
        }
        return sorted[left] == value;
    }

    /** Возвращает, отсортированную пузырьком, копию этого множества */
    int* sort() {
        int* arr1 = arr;
        int buffer;
        for (unsigned int i = 0; i < current_size - 1; i++) {
            for (unsigned int j = 0; j < current_size - i - 1; j++) {
                if (arr1[j] > arr1[j + 1]) {
                    buffer = arr1[j];
                    arr1[j] = arr1[j + 1];
                    arr1[j + 1] = buffer;
                }
            }
        }
        return arr1;
    }

    /** Очищает множество */
    void clear() {
        this->create(0);
    }

    /** Проверяет множество на пустоту */
    bool isEmpty() {
        return current_size == 0;
    }
};

// Пример
int main()
{
    Set *set = new Set();
    set->add(10);
    set->add(20);
    set->log();

    Set *set1 = new Set();
    set1->add(10);
    set1->add(20);
    set1->add(30);
    set1->log();

    set->append(set1);
    set->log();

    delete set;
    delete set1;
    return 0;
}
