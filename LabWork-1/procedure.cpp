#include <iostream>

/** Структура с информацией о множестве */
struct Set {
    unsigned int current_size;
    int* arr;
};

/** Cоздает динамический массив */
void create(Set &set, unsigned int size) {
    set.current_size = size;
    set.arr = new int[size];
};

/** Очищает множество */
void clear(Set &set) {
    create(set, 0);
};

/** Проверяет, пустое ли множество */
bool isEmpty(Set set) {
    return set.current_size == 0;
}

/** Возвращает элемент по индексу */
int get(Set set, unsigned int index) {
    if (index >= set.current_size)
        throw "OutOfBounds Exception";

    return set.arr[index];
};

/** Возвращает, отсортированную пузырьком, копию этого множества */
int* sort(Set set) {
    int* arr1 = set.arr;
    int buffer;
    for (unsigned int i = 0; i < set.current_size - 1; i++) {
        for (unsigned int j = 0; j < set.current_size - i - 1; j++) {
            if (arr1[j] > arr1[j + 1]) {
                buffer = arr1[j];
                arr1[j] = arr1[j + 1];
                arr1[j + 1] = buffer;
            }
        }
    }
    return arr1;
};

/** Проверяет наличие элемента в множестве */
bool contains(Set set, int value) {
    if (isEmpty(set))
        return false;

    int* sorted = sort(set);
    unsigned int left = 0;
    unsigned int right = set.current_size - 1;
    unsigned int middle;
    while (left < right) {
        middle = (left + right) / 2;

        if (sorted[middle] < value)
            left = middle + 1;
        else
            right = middle;
    }
    return sorted[left] == value;
};

/** Добавляет значение в множество */
void add(Set &set, int value) {
    if (contains(set, value))
        return;

    int* buffer = new int[set.current_size];
    buffer = set.arr;
    set.arr = new int[set.current_size + 1];

    for (unsigned int i = 0; i < set.current_size; i++)
        set.arr[i] = buffer[i];

    set.arr[set.current_size++] = value;
};

/** Пересоздает это множество из переданного */
void from(Set &setTo, Set setFrom) {
    clear(setTo);
    for (unsigned int i = 0; i < setFrom.current_size; i++) {
        add(setTo, setFrom.arr[i]);
    }
}

/** Добавляет переданное множество к этому */
void append(Set &setTo, Set setFrom) {
    for (unsigned int i = 0; i < setFrom.current_size; i++) {
        add(setTo, setFrom.arr[i]);
    }
}

/** Выводит элементы множества в консоль */
void log(Set set) {
    if (isEmpty(set)) {
        std::cout << "{ emtpty }" << std::endl;
        return;
    }

    std::cout << "{ ";
    for (unsigned int i = 0; i < set.current_size; i++){
        std::cout << set.arr[i] << (i + 1 == set.current_size ? "" : ", ");
    }
    std::cout << " }" << std::endl;
};

/** Очищает переданное множество */
void destruct(Set &set) {
    delete [] set.arr;
};

// Пример
int main()
{
    Set set = Set();
    add(set, 10);
    add(set,20);
    log(set);

    Set set1 = Set();
    add(set1, 10);
    add(set1, 20);
    add(set1, 30);
    log(set1);

    append(set, set1);
    log(set);

    destruct(set);
    destruct(set1);
    return 0;
}
