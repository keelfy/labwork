#include <iostream>
#include <list>

using namespace std;

class SortedSet {
private:
    /// @brief List of numbers
    std::list<int> list;

    /**
     * @brief Creates list by size and default values of elements.
     *
     * @param size - size of set (default is 0).
     * @param value - default values of elements (default is 0).
     */
    void create(unsigned int size = 0, int value = 0) {
        list.assign(size, value);
    }
public:
    /// @brief Creates instance of zero-sized set.
    SortedSet() {
        this->create(0);
    }

    /**
     * @brief Creates instance of sized set.
     *
     * @param size - size of set.
     * @param value - default values of elements (default is 0).
     */
    SortedSet(unsigned int size, int value = 0) {
        this->create(size, value);
    }

    /**
     * @brief Copies data from set to this
     *
     * @param set - set to copy
     */
    SortedSet(SortedSet& set) {
        this->from(set);
    }

    /**
     * @brief Copies data from set to this
     *
     * @param set - set to copy
     * @return this
     */
    SortedSet& from(SortedSet& set) {
        this->clear();
        for (unsigned int i = 0; i < set.size(); i++) {
            int el = set.get(i);

            if (this->contains(el))
                continue;

            this->add(el);
        }
        this->sort();
        return *this;
    }

    /**
     * @brief Appends values from set to this
     *
     * @param set - set to append
     * @return this
     */
    SortedSet& append(SortedSet& set) {
        for (unsigned int i = 0; i < set.size(); i++) {
            int el = set.get(i);

            if (this->contains(el))
                continue;

            this->add(el);
        }
        this->sort();
        return *this;
    }

    /// @brief Destructor clears the environment
    /// WE CAN SAVE THE PLANET FROM TRASH TOGETHER
    ~SortedSet() {
        clear();
    }

    /**
     * @brief Gets value by index of elements
     *
     * @param index - index of elements
     * @return value
     */
    int get(unsigned int index) {
        if (index >= size())
            throw std::out_of_range("OutOfBounds");

        std::list<int>::iterator it = list.begin();
        std::advance(it, index);
        return *it;
    }

    /**
     * @brief Sets element values by index
     *
     * @param index - index of elements
     * @param value - new value
     */
    void set(unsigned int index, int value) {
        if (index >= size())
            throw std::out_of_range("OutOfBounds");

        if (this->contains(value))
            return;

        std::list<int>::iterator it = list.begin();
        std::advance(it, index);
        this->list.erase(it);
        this->list.push_back(value);
        this->sort();
    }

    /**
     * @brief Inserts new value at the end of the set
     *
     * @param value - value to add
     */
    void add(int value) {
        if (this->contains(value))
            return;

        this->list.push_back(value);
        this->sort();
    }

    /**
     * @brief Gets the size of set
     *
     * @return size
     */
    unsigned int size() {
        return list.size();
    }

    /**
     * @brief Checks emptiness
     *
     * @return check result
     */
    bool isEmpty() {
        return list.empty();
    }

    /// @brief Clears the set
    void clear() {
        list.clear();
    }

    /**
     * @brief Checks existance of value in set
     *
     * @param value - value to check
     * @return check result
     */
    bool contains(int value) {
        if (isEmpty())
            return false;

        unsigned int left = 0;
        unsigned int right = this->size() - 1;
        unsigned int middle;
        while (left < right) {
            middle = (left + right) / 2;

            if (this->get(middle) < value)
                left = middle + 1;
            else
                right = middle;
        }
        return this->get(left) == value;
    }

    /**
     * @brief Sorts values
     *
     * @return this
     */
    SortedSet& sort() {
        this->list.sort();
        return *this;
    }

    /**
     * @brief Multiplies this by set
     *
     * @param set - set to multiply
     * @return result
     */
    SortedSet& multiply(SortedSet& set) {
        SortedSet *result = new SortedSet();
        for (unsigned int i = 0; i < this->size(); i++) {
            int el = this->get(i);
            if (set.contains(el))
                result->add(el);
        }
        return *result;
    }

    /**
     * @brief Subtracts from this set
     *
     * @param set - set to subtract from
     * @return result
     */
    SortedSet& subtract(SortedSet& set) {
        SortedSet *result = new SortedSet();
        for (unsigned int i = 0; i < this->size(); i++)
            if (!set.contains(this->get(i)))
                result->add(this->get(i));
        return *result;
    }

    SortedSet& operator +=(SortedSet& b) {
        if (this == &b)
            return *this;

        return this->append(b);
    }

    SortedSet& operator +=(int b) {
        this->add(b);
        return *this;
    }

    SortedSet& operator *=(SortedSet& b) {
        if (this == &b)
            return *this;

        return this->from(this->multiply(b));
    }

    SortedSet& operator -=(SortedSet& b) {
        if (this == &b)
            return *this;

        return this->from(subtract(b));
    }

    SortedSet& operator =(SortedSet& b) {
        if (this == &b)
            return *this;

        return this->from(b);
    }

    bool operator ==(SortedSet& b) {
        if (this->size() != b.size())
            return false;

        for (unsigned int i = 0; i < this->size(); i++)
            if (this->get(i) != b.get(i))
                return false;
        return true;
    }

    bool operator !=(SortedSet& b) {
        return !(this == &b);
    }

    bool operator <(SortedSet& b) {
        return this->size() < b.size();
    }

    bool operator >(SortedSet& b) {
        return this->size() > b.size();
    }

    bool operator <=(SortedSet& b) {
        return this->size() <= b.size();
    }

    bool operator >=(SortedSet& b) {
        return this->size() >= b.size();
    }

    int operator[](unsigned int index) {
        return get(index);
    }

    friend std::ostream& operator <<(std::ostream& os, SortedSet& set);
    friend SortedSet& operator +(SortedSet& a, SortedSet& b);
    friend SortedSet& operator +(SortedSet& a, int b);
    friend SortedSet& operator *(SortedSet& a, SortedSet& b);
    friend SortedSet& operator -(SortedSet& a, SortedSet& b);
};

SortedSet& operator +(SortedSet& a, SortedSet& b) {
    SortedSet *set = new SortedSet(a);
    return set->append(b);
}

SortedSet& operator +(SortedSet &a, int b) {
    SortedSet *set = new SortedSet(a);
    set->add(b);
    return *set;
}

SortedSet& operator *(SortedSet& a, SortedSet& b) {
    SortedSet *set = new SortedSet(a);
    return set->multiply(b);
}

SortedSet& operator -(SortedSet& a, SortedSet& b) {
    SortedSet *set = new SortedSet(a);
    return set->subtract(b);
}

std::ostream& operator <<(std::ostream& os, SortedSet& set) {
    if (set.isEmpty()) {
        os << "{ empty }" << std::endl;
        return os;
    }
    os << "{ ";
    for (unsigned int i = 0; i < set.size(); i++)
        os << set[i] << (i + 1 == set.size() ? "" : ", ");
    os << " }";
    return os;
}

/// Tests
int main()
{
    SortedSet set;
    set.add(400);
    set += 20;
    std::cout << "set_1 = " << set << std::endl;

    SortedSet set1;
    set1.add(10);
    set1.add(20);
    set1 += 15;
    std::cout << "set_2 = " << set1 << std::endl << std::endl;

    std::cout << "Is set_1 equal set_2? => " << ((set == set1) ? "True" : "False") << std::endl << std::endl;

    SortedSet set2;
    SortedSet set3;

    set2 = set * set1;
    std::cout << "set_1 * set_2 => " << set2 << std::endl;

    set3 = set1;
    set3 *= set;
    std::cout << "set_2 *= set_1 => " << set3 << std::endl;

    set2 = set + set1;
    std::cout << "set_1 + set_2 => "<< set2 << std::endl;

    set3 = set1;
    set3 += set;
    std::cout << "set_1 += set_2 => " << set3 << std::endl;

    set2 = set1 - set;
    std::cout << "set_2 - set_1 => "<< set2 << std::endl;

    set3 = set1;
    set3 -= set;
    std::cout << "set_2 -= set_1 => " << set3 << std::endl;

    return 0;
}
