#include <iostream>

class Set {
    unsigned int current_size;
    int* arr;

    /** Cоздает динамический массив */
    void create(unsigned int size) {
        this->current_size = size;
        this->arr = new int[size];
    }

public:
    /** Конструктор создает пустое множество */
    Set() {
        this->create(0);
    }

    Set(int* array, unsigned int size) {
        this->current_size = size;
        this->arr = array;
    }

    /** Деструктор удаляет массив */
    ~Set() {
        delete [] arr;
    }

    /** Пересоздает это множество из переданного */
    Set& from(Set* set) {
        this->clear();
        for (unsigned int i = 0; i < set->size(); i++) {
            this->add(set->get(i));
        }
        return *this;
    }

    /** Добавляет переданное множество к этому */
    Set& append(Set* set) {
        for (unsigned int i = 0; i < set->size(); i++)
            this->add(set->get(i));
        return *this;
    }

    /** Возвращает текущий размер списка */
    unsigned int size() {
        return current_size;
    }

    /** Возвращает элемент по индексу */
    int get(unsigned int index) {
        if (index >= current_size)
            throw "OutOfBounds Exception";
        return this->arr[index];
    }

    /** Добавляет значение в множество */
    Set& add(int value) {
        if (this->contains(value))
            return *this;

        int* buffer = new int[current_size];
        buffer = arr;
        this->arr = new int[current_size + 1];

        for (unsigned int i = 0; i < current_size; i++)
            this->arr[i] = buffer[i];

        arr[current_size++] = value;
        return *this;
    }

    /** Проверяет наличие элемента в множестве */
    bool contains(int value) {
        if (isEmpty())
            return false;

        Set sorted = sort();
        unsigned int left = 0;
        unsigned int right = size() - 1;
        unsigned int middle;
        while (left < right) {
            middle = (left + right) / 2;

            if (sorted.get(middle) < value)
                left = middle + 1;
            else
                right = middle;
        }
        return sorted.get(left) == value;
    }

    /** Возвращает, отсортированную пузырьком, копию этого множества */
    Set& sort() {
        int* arr1 = arr;
        int buffer;
        for (unsigned int i = 0; i < this->size() - 1; i++) {
            for (unsigned int j = 0; j < this->size() - i - 1; j++) {
                if (arr1[j] > arr1[j + 1]) {
                    buffer = arr1[j];
                    arr1[j] = arr1[j + 1];
                    arr1[j + 1] = buffer;
                }
            }
        }
        return *new Set(arr1, this->size());
    }

    /** Возвращает новое множество, которое получается после умножения этого на переданное */
    Set& multiply(Set* set) {
        Set* result = new Set();
        for (unsigned int i = 0; i < this->size(); i++)
            if (set->contains(this->get(i)))
                result->add(this->get(i));
        return *result;
    }

    /** Возвращает новое множество, которое получается после вычитания из этого переданного */
    Set& subtract(Set* set) {
        Set* result = new Set();
        for (unsigned int i = 0; i < this->size(); i++)
            if (!set->contains(this->get(i)))
                result->add(this->get(i));
        return *result;
    }

    /** Очищает список */
    Set& clear() {
        this->create(0);
        return *this;
    }

    /** Проверяет множество на пустоту */
    bool isEmpty() {
        return current_size == 0;
    }

    /** Перегрузка оператора сложения - логическое ИЛИ */
    Set operator +(Set b) {
        Set set = *this;
        return set.append(&b);
    }

    /** Перегрузка сложения с множеством с присвоением */
    Set operator +=(Set b) {
        return this->append(&b);
    }

    /** Перегрузка оператора сложения с числом - добавление элемента в множество */
    Set operator +(int b) {
        Set set = *this;
        return set.add(b);
    }

    /** Перегрузка сложения с числом с присвоением */
    Set operator +=(int b) {
        return this->add(b);
    }

    /** Перегрузка оператора умножения - логическое И */
    Set operator *(Set b) {
        Set set = *this;
        return set.multiply(&b);
    }

    /** Перегрузка умножения с присвоением */
    Set operator *=(Set b) {
        return this->from(&this->multiply(&b));
    }

    /** Перегрузка оператора вычитания - возвращает значения первого множества, которых нет во втором */
    Set operator -(Set b) {
        return this->subtract(&b);
    }

    /** Перегрузка вычитания с присвоением */
    Set operator -=(Set b) {
        return this->from(&this->subtract(&b));
    }

    /** Перегрузка оператора присвоения множества */
    Set operator =(Set b) {
        if (this == &b)
            return *this;

        this->from(&b);
        return *this;
    }

    /** Перегрузка эквивалентности */
    bool operator ==(Set b) {
        return this->size() == b.size();
    }

    /** Перегрузка неэквивалентности */
    bool operator !=(Set b) {
        return this->size() != b.size();
    }

    /** Перегрузка оператора меньше */
    bool operator <(Set b) {
        return this->size() < b.size();
    }

    /** Перегрузка оператора больше */
    bool operator >(Set b) {
        return this->size() > b.size();
    }

    /** Перегрузка оператора меньше или равно */
    bool operator <=(Set b) {
        return this->size() <= b.size();
    }

    /** Перегрузка оператора больше или равно */
    bool operator >=(Set b) {
        return this->size() >= b.size();
    }

    friend std::ostream& operator<<(std::ostream& os, const Set& set);
};

/** Перегрузка оператора потокового вывода множества */
std::ostream& operator<<(std::ostream& os, Set& set) {
    if (set.isEmpty()) {
        os << "{ empty }" << std::endl;
        return os;
    }
    os << "{ ";
    for (unsigned int i = 0; i < set.size(); i++)
        os << set.get(i) << (i + 1 == set.size()? "" : ", ");
    os << " }";
    return os;
};

// Пример
int main()
{
    Set set;
    set.add(10);
    set += 20;
    std::cout << "Set 1 = " << set << std::endl;

    Set set1;
    set1.add(10);
    set1.add(20);
    set1 += 15;
    std::cout << "Set 2 = " << set1 << std::endl << std::endl;

    Set set2 = *new Set();

    set2 = set * set1;
    std::cout << "Mul = " << set2 << std::endl;

    set2 = set + set1;
    std::cout << "Sum = "<< set2 << std::endl;

    set1 -= set;
    std::cout << "Sub = "<< set1 << std::endl;

    delete &set;
    delete &set1;
    delete &set2;
    return 0;
}
